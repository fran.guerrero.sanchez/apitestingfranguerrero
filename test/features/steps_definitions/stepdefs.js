const {Given,When,Then,And} = require('cucumber');
const { expect } = require("chai");
const axios = require('axios');
const jsonData = require('../../../env.json');
const lookup = require('../../utils/utils');
let res;


//Steps Definitions
Given('I call the punk api with {string} id {int}', async function (resource,id) {
  var headers= {
    'Content-Type': 'application/json'
  }

  //Using Axios FW and building request with baseUrl+endpoint+parameter
  res = await axios.get(`${jsonData.basePunkApiUrl}/${resource}/${id}`,{headers:headers})
  .then(function(response){
    return response;
  })
  .catch(function(error){  
     console.log(error.response);
  });
});

Then('I expect {int} status response', async function (status) {
  expect (await res.status).equals(status);
});

Then('the {string} is {string}', async function(element,name) {
  var rs = lookup(res.data['0'],element)
  expect (rs['0'].name).equals(name)
});

Then('the {string} value is {float} and the unit is {string}', async function(element,amount, unit) {
  var rs = lookup(res.data['0'],element)
  expect (rs['0'].amount.value).equals(amount)
  expect (rs['0'].amount.unit).equals(unit)
});