# syntax=docker/dockerfile:1


FROM node:18 as base

# Create test directory
WORKDIR /

# Install app dependencies
COPY package.json ./
RUN npm install


# Tests source
COPY . .
RUN npm run test

EXPOSE 8080
CMD [ "npm", "test" ]
